﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuddleAzureService.TableUtils;
using Microsoft.WindowsAzure.Storage.Table;
using WorkerRole1.StorageTypes;

namespace WorkerRole1.TableUtils
{
    public static class UserEntityUtils
    {
        /// <summary>
        /// Looks up a Huddle User ID and returns the Huddle user entity.
        /// </summary>
        /// <param name="huddleUserId"></param>
        /// <returns></returns>
        public static async Task<UserEntity> GetUserEntity(string userId)
        {
            CloudTable usersTable = await GetCreateTable.GetTable("users");

            TableOperation retrieveOperation = TableOperation.Retrieve<UserEntity>(userId, userId);

            TableResult result = await usersTable.ExecuteAsync(retrieveOperation);

            return (UserEntity)result.Result;
        }

        /// <summary>
        /// Looks up a Huddle User ID and the device Id and returns the Huddle Device entity.
        /// </summary>
        /// <param name="huddleUserId"></param>
        /// <returns></returns>
        public static async Task<DeviceEntity> GetDeviceEntity(string userId, string deviceId)
        {
            CloudTable usersTable = await GetCreateTable.GetTable("devices");

            TableOperation retrieveOperation = TableOperation.Retrieve<DeviceEntity>(userId, deviceId);

            TableResult result = await usersTable.ExecuteAsync(retrieveOperation);

            return (DeviceEntity)result.Result;
        }

        public static async Task<List<UserEntity>> GetUserObjectsFromIds(List<String> userIds)
        {
            List<Task<UserEntity>> getUserEntitiesTasks = new List<Task<UserEntity>>();

            foreach (string userId in userIds)
            {
                if (!string.IsNullOrEmpty(userId))
                {
                    getUserEntitiesTasks.Add(GetUserEntity(userId));
                }
            }

            return (await Task.WhenAll(getUserEntitiesTasks.ToArray())).ToList();
        }

        /// <summary>
        /// Get DeviceIds for a given user
        /// </summary>
        /// <param name="huddleUserId">HuddleId of the user to look up devices for</param>
        /// <returns>List of device ids</returns>
        public static async Task<List<string>> GetDevicesIdsForUser(string huddleUserId)
        {
            CloudTable userToHuddleIndex = await GetCreateTable.GetTable("devices");

            TableQuery<DeviceEntity> query = new TableQuery<DeviceEntity>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, huddleUserId));

            IEnumerable<DeviceEntity> entitiesForUser = userToHuddleIndex.ExecuteQuery(query);

            return (from deviceEntity in entitiesForUser
                    select deviceEntity.DeviceId).ToList();
        }

        /// <summary>
        /// Resolves the username -> userId
        /// </summary>
        /// <param name="userName">The username.</param>
        /// <returns></returns>
        public static async Task<UserNameToUserIdEntity> GetUserNameToUserIdEntity(string userName)
        {
            CloudTable usersTable = await GetCreateTable.GetTable("userNameToPusr");

            TableOperation retrieveOperation = TableOperation.Retrieve<UserNameToUserIdEntity>(userName, userName);

            TableResult result = await usersTable.ExecuteAsync(retrieveOperation);

            return (UserNameToUserIdEntity)result.Result;
        }

        /// <summary>
        /// Add a username -> Huddle User ID entry
        /// </summary>
        /// <param name="userName">The username.</param>
        /// <param name="userId">The HUSR that it maps to</param>
        /// <returns></returns>
        public static async Task SetUserNameToUserIdEntity(string userName, String userId)
        {
            CloudTable userNameToHusrTable = await GetCreateTable.GetTable("userNameToPusr");
            UserNameToUserIdEntity newUserNameToUserEntity = new UserNameToUserIdEntity(userName, userId);
            TableOperation insertOperation = TableOperation.Insert(newUserNameToUserEntity);
            await userNameToHusrTable.ExecuteAsync(insertOperation);
        }

        /// <summary>
        /// Helper to create a user entity
        /// </summary>
        /// <param name="msaUserId"></param>
        /// <param name="isAnonymous">If the user has ever logged in</param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="name"></param>
        /// <param name="gender"></param>
        /// <param name="locale"></param>
        /// <returns>The new user entity</returns>
        public static async Task<UserEntity> CreateUserEntity(
            String name)
        {
            CloudTable usersTable = await GetCreateTable.GetTable("users");

            UserEntity result = null;

            int count = 0;

            // TODO (gremit or peturner or someone brave): Clean this up and share it with the other uses of this pattern.
            // The idea here is to retry the operation a few times and fail after that.
            while (result == null && count < 30)
            {
                count++;

                string userId = "PUSR_" + Guid.NewGuid().ToString();

                UserEntity tempUserEntity = new UserEntity(userId, name);

                TableOperation insertOperation = TableOperation.Insert(tempUserEntity);

                try
                {
                    await usersTable.ExecuteAsync(insertOperation);

                    // We've successfully entered the new user, exit the loop
                    result = tempUserEntity;
                }
                catch { }
            }

            return result;
        }
    }
}
