﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuddleAzureService.TableUtils;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using WorkerRole1.AppLogic;
using WorkerRole1.StorageTypes;

namespace WorkerRole1.TableUtils
{
    class RoundEntityUtils
    {
        public static async Task<RoundEntity> CreateRoundEntity(RoomEntity room, SessionState sessionState, string type, string text)
        {
            CloudTable roomsTable = await GetCreateTable.GetTable("rounds");

            RoundEntity retVal = null;

            int retryCount = 0;
            while (retVal == null && retryCount++ < 5)
            {
                string roundId = "RND_" + Guid.NewGuid().ToString();

                RoundEntity tmpRoundEntity = new RoundEntity(roundId, room.RoomName, type, text);
                TableOperation insertOperation = TableOperation.Insert(tmpRoundEntity);

                try
                {
                    await roomsTable.ExecuteAsync(insertOperation);

                    // We've successfully entered the new user, exit the loop
                    retVal = tmpRoundEntity;
                }
                catch { }
            }

            return retVal;
        }

        public static async Task<RoundEntity> GetRoundEntity(string roomName, string roundId)
        {
            CloudTable huddlesTable = await GetCreateTable.GetTable("rounds");

            TableOperation retrieveOperation = TableOperation.Retrieve<RoundEntity>(roomName, roundId);

            TableResult result = await huddlesTable.ExecuteAsync(retrieveOperation);

            return (RoundEntity)result.Result;
        }

        public static async Task<RoundEntity> SaveRoundEntity(RoundEntity roundEntity)
        {
            CloudTable roomsTable = await GetCreateTable.GetTable("rounds");

            TableOperation saveOperation = TableOperation.Replace(roundEntity);

            TableResult result = await roomsTable.ExecuteAsync(saveOperation);

            return (RoundEntity)result.Result;
        }

        public static async Task CloseRound(string roomName, string roundId)
        {
            RoundEntity round = await GetRoundEntity(roomName, roundId);

            round.IsOpen = false;

            await SaveRoundEntity(round);
        }

        private class RoundClosedNotification
        {
            public string method = "round_closed";

            [JsonProperty("params")]
            public RoundClosedNotificationParams parameters;

            public RoundClosedNotification(string roundId)
            {
                this.parameters = new RoundClosedNotificationParams(roundId);
            }

        }

        private class RoundClosedNotificationParams
        {
            public RoundClosedNotificationParams(string roundId)
            {
                this.roundId = roundId;
            }

            public string roundId;
        }

        public static async Task SendRoundClosedNotifications(RoomEntity room, SessionState sessionState, string roundId, bool pushToCurrentDevice)
        {
            RoundClosedNotification notification = new RoundClosedNotification(roundId);

            await MethodUtils.DispatchNotificationToUsersAsync(room.GetUsers(), sessionState, JsonConvert.SerializeObject(notification), pushToCurrentDevice);
        }
    }
}
