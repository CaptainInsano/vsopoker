﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuddleAzureService.TableUtils;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using WorkerRole1.AppLogic;
using WorkerRole1.ErrorHandling;
using WorkerRole1.StorageTypes;

namespace WorkerRole1.TableUtils
{
    class RoomEntityUtils
    {
        public static async Task<RoomEntity> GetCreateRoomEntity(String roomName, SessionState sessionState, UserEntity currentUserEntity)
        {
            CloudTable roomsTable = await GetCreateTable.GetTable("rooms");

            RoomEntity retVal = null;

            int retryCount = 0;
            while (retVal == null && retryCount++ < 5)
            {
                try
                {
                    retVal = await GetRoomEntity(roomName);

                    if (retVal == null)
                    {
                        RoomEntity tempRoom = new RoomEntity(roomName, sessionState.UserId);

                        TableOperation insertOperation = TableOperation.Insert(tempRoom);

                        await roomsTable.ExecuteAsync(insertOperation);
                    }
                    else
                    {
                        await AddUserToRoom(retVal, currentUserEntity, sessionState);
                    }

                    return retVal;
                }
                catch { }
            }

            return retVal;
        }

        public static async Task<RoomEntity> GetRoomEntity(string roomName)
        {
            CloudTable huddlesTable = await GetCreateTable.GetTable("rooms");

            TableOperation retrieveOperation = TableOperation.Retrieve<RoomEntity>(roomName, roomName);

            TableResult result = await huddlesTable.ExecuteAsync(retrieveOperation);

            return (RoomEntity)result.Result;
        }

        public static async Task<RoomEntity> SaveRoomEntity(RoomEntity roomEntity)
        {
            CloudTable roomsTable = await GetCreateTable.GetTable("rooms");

            TableOperation saveOperation = TableOperation.Replace(roomEntity);

            TableResult result = await roomsTable.ExecuteAsync(saveOperation);

            return (RoomEntity)result.Result;
        }

        private class UserJoinedNotification
        {
            public string method = "user_joined";

            [JsonProperty("params")]
            public UserJoinedNotificationParams parameters;

            public UserJoinedNotification(UserEntity user, string roundId, string roomName)
            {
                this.parameters = new UserJoinedNotificationParams(user, roundId, roomName);
            }

        }

        private class UserJoinedNotificationParams
        {
            public UserJoinedNotificationParams(UserEntity user, string roundId, string roomName)
            {
                this.user = user;
                this.roundId = roundId;
                this.roomName = roomName;
            }

            public string roundId;
            public string roomName;
            public UserEntity user;
        }

        public static async Task AddUserToRoom(RoomEntity room, UserEntity currentUser, SessionState sessionState)
        {
            List<String> usersList = room.GetUsers();
           
            if (!usersList.Contains(currentUser.UserId))
            {
                usersList.Add(currentUser.UserId);
            }
            
            room.SetUsers(usersList);

            await SaveRoomEntity(room);

            UserJoinedNotification notification = new UserJoinedNotification(currentUser, room.CurrentRoundId, room.RoomName);

            MethodUtils.DispatchNotificationToUsersAsync(room.GetUsers(), sessionState, JsonConvert.SerializeObject(notification));
        }
    }
}
