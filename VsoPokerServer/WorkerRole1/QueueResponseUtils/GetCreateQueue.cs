﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.WindowsAzure.Storage.Table;

namespace WorkerRole1.QueueResponseUtils
{
    public static class GetCreateQueue
    {
        public static async Task<CloudQueue> GetQueue(string name)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("VSOPokerStorageConnectionString"));
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();

            // Azure queues must be lowercase and may not contain "_"
            string queueName = name.ToLower().Replace("_", "-");

            CloudQueue queue = queueClient.GetQueueReference(queueName);

            await queue.CreateIfNotExistsAsync();

            return queue;
        }
    }
}
