﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Queue;

namespace WorkerRole1.QueueResponseUtils
{
    class QueueResponseUtils
    {
        public static async Task PushQueueOntoReponseStream(SessionState sessionState, StreamWriter writer, string initialMessage)
        {
            if (String.IsNullOrEmpty(sessionState.DeviceId))
            {
                throw new InvalidOperationException("You must log in before executing operations");
            }

            if (sessionState.CancellationToken == null)
            {
                throw new InvalidOperationException("You should have Cancellation Token set!");
            }
            // This is the message we need to send to response stream before any other queued up messages in user queue
            if (initialMessage != null)
            {
                await writer.WriteAsync(initialMessage);
                await writer.FlushAsync();
            }
            CloudQueue queue = await GetCreateQueue.GetQueue(sessionState.DeviceId);
            CancellationToken token = sessionState.CancellationToken;

            while (!sessionState.IsDisposed)
            {
                CloudQueueMessage message = await queue.PeekMessageAsync(token);

                if (message != null)
                {
                    message = await queue.GetMessageAsync(token);

                    if (message != null)
                    {
                        await writer.WriteAsync(message.AsString);
                        await writer.FlushAsync();

                        await queue.DeleteMessageAsync(message, token);
                    }
                    else
                    {
                        await Task.Delay(300);
                    }
                }
                else
                {
                    await Task.Delay(300);
                }
            }
        }

        public static async Task PushMessageOntoUserQueue(string message, SessionState sessionState)
        {
            await PushMessageOntoUserQueue(message, sessionState.DeviceId);
        }

        public static async Task PushMessageOntoUserQueue(string message, string deviceId)
        {
            CloudQueue queue = await GetCreateQueue.GetQueue(deviceId);

            await queue.AddMessageAsync(new CloudQueueMessage(message));
        }
    }
}
