﻿using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkerRole1.StorageTypes
{
    [JsonObject(MemberSerialization.OptIn)]
    public class RoomEntity : TableEntity
    {
        public RoomEntity(string roomName, string currentUserId)
        {
            this.PartitionKey = roomName;
            this.RowKey = roomName;

            this.RoomName = roomName;

            this.Type = (int)EntityType.Room;

            SetUsers(new List<String>() { currentUserId } );
        }

        public RoomEntity() { }

        [JsonProperty("roomName")]
        public String RoomName { get; set; }

        [JsonProperty("currentRoundId")]
        public String CurrentRoundId { get; set; }

        [JsonProperty("users")]
        [JsonConverter(typeof(WriteThrough))]
        public String Users { get; set; }

        public List<String> GetUsers()
        {
            return ((List<String>)JsonConvert.DeserializeObject(Users ?? "[]", typeof(List<String>))).Where(item => !string.IsNullOrEmpty(item)).ToList();
        }

        public void SetUsers(List<String> knownUsers)
        {
            Users = JsonConvert.SerializeObject(knownUsers ?? new List<String>(), Formatting.None);
        }

        public int Type;
    }
}
