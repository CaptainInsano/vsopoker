﻿using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkerRole1.StorageTypes
{
    public class DeviceEntity : TableEntity
    {
        public DeviceEntity(string userId, string deviceId, string sequenceId = "")
        {
            this.PartitionKey = userId;
            this.RowKey = deviceId;
            this.DeviceId = deviceId;
            this.SequenceId = sequenceId;
        }

        public DeviceEntity() { }

        public string SequenceId { get; set; }

        public string DeviceId { get; set; }
    }
}
