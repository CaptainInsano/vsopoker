﻿using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkerRole1.StorageTypes
{
    [JsonObject(MemberSerialization.OptIn)]
    public class RoundEntity : TableEntity
    {
        public RoundEntity(string roundId, string roomName, string type, string text)
        {
            this.PartitionKey = roomName;
            this.RowKey = roundId; ;

            this.RoundId = roundId;
            this.RoomName = roomName;
            this.IsOpen = true;
            this.VsoType = type;
            this.Text = text;

            this.Type = (int)EntityType.Round;

            SetVotes(new Dictionary<String, int>());
        }

        public RoundEntity() { }

        [JsonProperty("roomName")]
        public String RoomName { get; set; }

        [JsonProperty("roundId")]
        public String RoundId { get; set; }

        [JsonProperty("isOpen")]
        public Boolean IsOpen { get; set; }

        [JsonProperty("votes")]
        [JsonConverter(typeof(WriteThrough))]
        public String Votes { get; set; }
        public int Type { get; set; }

        [JsonProperty("type")]
        public String VsoType { get; set; }

        [JsonProperty("text")]
        public String Text { get; set; }

        public Dictionary<String, int> GetVotes()
        {
            return (Dictionary<String, int>)JsonConvert.DeserializeObject(Votes ?? "{}", typeof(Dictionary<String, int>));
        }

        public void SetVotes(Dictionary<String, int> votes)
        {
            Votes = JsonConvert.SerializeObject(votes ?? new Dictionary<String, int>(), Formatting.None);
        }
    }
}
