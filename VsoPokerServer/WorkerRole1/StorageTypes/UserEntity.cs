﻿using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkerRole1.StorageTypes
{
    [JsonObject(MemberSerialization.OptIn)]
    public class UserEntity : TableEntity
    {
        public UserEntity(string userId, string userName = "")
        {
            this.PartitionKey = userId;
            this.RowKey = userId;

            this.UserId = userId;
            this.UserName = userName;
        }

        public UserEntity() { }

        [JsonProperty("userId")]
        public String UserId { get; set; }

        [JsonProperty("userName")]
        public String UserName { get; set; }
    }
}
