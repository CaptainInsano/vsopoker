﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkerRole1.StorageTypes
{
    public class UserNameToUserIdEntity : TableEntity
    {
        public string UserId { get; set; }

        public UserNameToUserIdEntity() { }

        public UserNameToUserIdEntity(string userName, string userId)
        {
            this.PartitionKey = userName;
            this.RowKey = userName;
            this.UserId = userId;
        }
    }
}
