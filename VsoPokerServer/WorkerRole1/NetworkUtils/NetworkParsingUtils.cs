﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace WorkerRole1.NetworkUtils
{
    public static class NetworkParsingUtils
    {
        /// <summary>
        /// This just does { parsing 
        /// TODO (peturner or gremit or someone brave): Rewrite this.
        /// </summary>
        /// <param name="sr"></param>
        /// <returns></returns>
        public async static Task<string> GetJsonObjectFromStream(StreamReader sr)
        {
            StringBuilder inputStreamStringBuilder = new StringBuilder();

            int parenCount = 0;
            bool hasReadAnything = false;

            char[] buff = new char[1];

            while (!hasReadAnything || parenCount > 0)
            {
                int readCount = await sr.ReadAsync(buff, 0, 1);

                if (readCount == 0)
                {
                    throw new Exception("ReadCount of 0 on the socket");
                }

                char c = buff[0];

                // Skip everything between brackets
                if (c != '{' && c != '}' && !hasReadAnything)
                {
                    continue;
                }

                inputStreamStringBuilder.Append(c);

                hasReadAnything = true;

                if (c == '{')
                {
                    parenCount++;
                }
                else if (c == '}')
                {
                    parenCount--;
                }
            }

            return inputStreamStringBuilder.ToString();
        }

    }
}
