﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WorkerRole1.AppLogic;
using WorkerRole1.StorageTypes;
using WorkerRole1.TableUtils;

namespace WorkerRole1.ErrorHandling
{
    public static class UserDisconnectUtils
    {
        public static async Task HandleUserDisconnect(SessionState sessionState)
        {
            if (sessionState.RoomName != null)
            {
                int retry = 5;
                while (retry-- > 0)
                {
                    RoomEntity room = await RoomEntityUtils.GetRoomEntity(sessionState.RoomName);

                    List<String> users = room.GetUsers();

                    users.Remove(sessionState.UserId);

                    room.SetUsers(users);

                    await RoomEntityUtils.SaveRoomEntity(room);

                    UserLeftNotification notification = new UserLeftNotification(sessionState.UserId, room.RoomName, sessionState.UserName);

                    MethodUtils.DispatchNotificationToUsersAsync(room.GetUsers(), sessionState, JsonConvert.SerializeObject(notification));

                    return;
                }
            }
        }

        private class UserLeftNotification
        {
            public string method = "user_left";

            [JsonProperty("params")]
            public UserLeftNotificationParams parameters;

            public UserLeftNotification(String userId, string roomName, string userName)
            {
                this.parameters = new UserLeftNotificationParams(userId, roomName, userName);
            }

        }

        private class UserLeftNotificationParams
        {
            public UserLeftNotificationParams(String userId, string roomName, string userName)
            {
                this.userId = userId;
                this.roomName = roomName;
                this.userName = userName;
            }

            public string userId;
            public string roomName;
            public string userName;
        }

    }
}
