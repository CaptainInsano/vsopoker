﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WorkerRole1.ErrorHandling
{
    public static class ErrorHandlingUtils
    {
        private class RpcErrorResponse
        {
            public string jsonrpc = "2.0";
            public string id;
            public string error;
        }

        public static void HandleGenericErrorAndCloseSession(SessionState sessionState, Exception e, StreamWriter streamWriter)
        {
            HandleGenericError(sessionState, e, streamWriter);
            sessionState.EndSession();
        }

        public static void HandleGenericError(SessionState sessionState, Exception e, StreamWriter streamWriter)
        {
            RpcErrorResponse errorResponse = new RpcErrorResponse();

            errorResponse.id = sessionState.CurrentRequestId;

            while (e is AggregateException)
            {
                e = e.InnerException;
            }

            errorResponse.error = e != null ?
                                    e.Message :
                                    "Unkown server error";

            string responseMessage = JsonConvert.SerializeObject(errorResponse);

            streamWriter.Write(responseMessage);
            streamWriter.Flush();
        }
    }
}
