﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkerRole1.StorageTypes;
using WorkerRole1.TableUtils;

namespace WorkerRole1.ResponseTypes
{
    public class RoundResponse
    {
        public string roundId;
        public bool isOpen;
        public List<UserEntity> users;
        public Dictionary<String, int> votes;

        public RoundResponse() {}

        public async static Task<RoundResponse> GetCurrentRoundResponse(RoomEntity room)
        {
            RoundResponse response = new RoundResponse();

            response.roundId = room.CurrentRoundId;

            response.users = await UserEntityUtils.GetUserObjectsFromIds(room.GetUsers());

            if (room.CurrentRoundId != null)
            {
                RoundEntity round = await RoundEntityUtils.GetRoundEntity(room.RoomName, room.CurrentRoundId);

                response.votes = round.GetVotes();
                response.isOpen = round.IsOpen;
            }

            return response;
        }
    }
}
