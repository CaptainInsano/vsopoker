﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WorkerRole1.TableUtils;
using WorkerRole1.StorageTypes;
using HuddleAzureService.TableUtils;
using Microsoft.WindowsAzure.Storage.Table;
using System.Threading;
using WorkerRole1.ErrorHandling;

namespace WorkerRole1.AppLogic
{
    public partial class Methods
    {
        private class VoteOnRoundMethodParser
        {
            public string jsonrpc;
            public string method;

            [JsonProperty("params")]
            public VoteOnRoundParametersParser parameters;
        }

        private class VoteOnRoundParametersParser
        {
            public string roundId;
            public int voteVal;
        }

        public class VoteOnRoundResponse : IRpcResponse
        {
        }

        /// <summary>
        /// Logs the user in, and, if necessary, creates the user in the user table.
        /// </summary>
        /// <param name="methodJson">The JSON representing the input parameters to the login method.</param>
        /// <param name="sessionState">The user's session state.</param>
        /// <param name="writer">The StreamWriter into which the response should be written.</param>
        /// <returns>A JSON-RPC response indicating success or failure.</returns>
        public static async Task<IRpcResponse> VoteOnRound(string methodJson, SessionState sessionState)
        {
            VoteOnRoundMethodParser voteOnRoundMethod = (VoteOnRoundMethodParser)JsonConvert.DeserializeObject(methodJson, typeof(VoteOnRoundMethodParser));

            RoomEntity room = await RoomEntityUtils.GetRoomEntity(sessionState.RoomName);

            if (room == null)
            {
                throw new Exception("Current room could not be found: " + sessionState.RoomName);
            }

            if (room.CurrentRoundId != voteOnRoundMethod.parameters.roundId)
            {
                throw new Exception("Not Voting on the current roundId: " + room.CurrentRoundId);
            }

            RoundEntity round = await RoundEntityUtils.GetRoundEntity(room.RoomName, room.CurrentRoundId);

            if (!round.IsOpen)
            {
                throw new Exception("The current round is closed: " + room.CurrentRoundId);
            }

            // Change vote value
            Dictionary<String, int> votes = round.GetVotes();
            votes[sessionState.UserId] = voteOnRoundMethod.parameters.voteVal;
            round.SetVotes(votes);
            await RoundEntityUtils.SaveRoundEntity(round);

            // check to see if all users have voted, and close the round if so
            if (HaveAllUsersVoted(room.GetUsers(), votes))
            {
                RoundEntity roundForClosing = await RoundEntityUtils.GetRoundEntity(room.RoomName, room.CurrentRoundId);

                roundForClosing.IsOpen = false;

                await RoundEntityUtils.SaveRoundEntity(roundForClosing);

                RoundEntityUtils.SendRoundClosedNotifications(room, sessionState, roundForClosing.RoundId, pushToCurrentDevice: true);
            }

            VoteChangedNotification notification = new VoteChangedNotification(round.RoundId, sessionState.UserId, sessionState.RoomName, voteOnRoundMethod.parameters.voteVal);

            MethodUtils.DispatchNotificationToUsersAsync(room.GetUsers(), sessionState, JsonConvert.SerializeObject(notification));

            return new VoteOnRoundResponse() { };
        }

        private static bool HaveAllUsersVoted(List<string> userIds, Dictionary<String, int> votes)
        {

            foreach (string userId in userIds)
            {
                if (!votes.ContainsKey(userId) || votes[userId] < 0)
                {
                    return false;
                }
            }
            
            return true;
        }

        
        private class VoteChangedNotification
        {
            public string method = "vote_changed";

            [JsonProperty("params")]
            public VoteChangedNotificationParams parameters;

            public VoteChangedNotification(string roundId, string userId, string roomName, int voteVal)
            {
                this.parameters = new VoteChangedNotificationParams(roundId, userId, roomName, voteVal);
            }

        }

        private class VoteChangedNotificationParams
        {
            public VoteChangedNotificationParams(string roundId, string userId, string roomName, int voteVal)
            {
                this.roundId = roundId;
                this.userId = userId;
                this.roomName = roomName;
                this.voteVal = voteVal;
            }

            public string roundId;
            public string userId;
            public string roomName;
            public int voteVal;
        }
        
    }
}
