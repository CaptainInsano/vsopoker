﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WorkerRole1.TableUtils;
using WorkerRole1.StorageTypes;
using HuddleAzureService.TableUtils;
using Microsoft.WindowsAzure.Storage.Table;
using System.Threading;
using WorkerRole1.ErrorHandling;

namespace WorkerRole1.AppLogic
{
    public partial class Methods
    {
        private class CloseRoundMethodParser
        {
            public string jsonrpc;
            public string method;

            [JsonProperty("params")]
            public CloseRoundParametersParser parameters;
        }

        private class CloseRoundParametersParser
        {
            public string roundId;
        }

        public class CloseRoundResponse : IRpcResponse
        {
        }

        /// <summary>
        /// Logs the user in, and, if necessary, creates the user in the user table.
        /// </summary>
        /// <param name="methodJson">The JSON representing the input parameters to the login method.</param>
        /// <param name="sessionState">The user's session state.</param>
        /// <param name="writer">The StreamWriter into which the response should be written.</param>
        /// <returns>A JSON-RPC response indicating success or failure.</returns>
        public static async Task<IRpcResponse> CloseRound(string methodJson, SessionState sessionState)
        {
            CloseRoundMethodParser closeRoundMethod = (CloseRoundMethodParser)JsonConvert.DeserializeObject(methodJson, typeof(CloseRoundMethodParser));

            RoomEntity room = await RoomEntityUtils.GetRoomEntity(sessionState.RoomName);

            if (room == null)
            {
                throw new Exception("Current room could not be found: " + sessionState.RoomName);
            }

            if (room.CurrentRoundId != closeRoundMethod.parameters.roundId)
            {
                throw new Exception("Not trying to close the current roundId: " + room.CurrentRoundId);
            }

            RoundEntity round = await RoundEntityUtils.GetRoundEntity(room.RoomName, room.CurrentRoundId);

            if (!round.IsOpen)
            {
                throw new Exception("The current round is already closed: " + room.CurrentRoundId);
            }

            round.IsOpen = false;

            await RoundEntityUtils.SaveRoundEntity(round);

            RoundEntityUtils.SendRoundClosedNotifications(room, sessionState, round.RoundId, pushToCurrentDevice: false);

            return new VoteOnRoundResponse() { };
        }
    }
}
