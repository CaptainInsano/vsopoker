﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WorkerRole1.TableUtils;
using WorkerRole1.StorageTypes;
using HuddleAzureService.TableUtils;
using Microsoft.WindowsAzure.Storage.Table;
using System.Threading;
using WorkerRole1.ErrorHandling;
using WorkerRole1.ResponseTypes;

namespace WorkerRole1.AppLogic
{
    public partial class Methods
    {
        private class LoginMethodParser
        {
            public string jsonrpc;
            public string method;

            [JsonProperty("params")]
            public LoginParametersParser parameters;
        }

        private class LoginParametersParser
        {
            public string userName;
            public string deviceId;
            public string roomName;
        }

        public class LoginResponse : IRpcResponse
        {
            public string deviceId;
            public UserEntity user;
            public RoundResponse round;
        }

        /// <summary>
        /// Logs the user in, and, if necessary, creates the user in the user table.
        /// </summary>
        /// <param name="methodJson">The JSON representing the input parameters to the login method.</param>
        /// <param name="sessionState">The user's session state.</param>
        /// <param name="writer">The StreamWriter into which the response should be written.</param>
        /// <returns>A JSON-RPC response indicating success or failure.</returns>
        public static async Task<IRpcResponse> Login(string methodJson, SessionState sessionState, StreamWriter writer)
        {
            LoginMethodParser loginMethod = (LoginMethodParser)JsonConvert.DeserializeObject(methodJson, typeof(LoginMethodParser));

            string userName = loginMethod.parameters.userName;

            // Look up the user name we got in the username -> Huddle User ID mapping (i.e., see if this
            // user is already a Huddler).
            UserNameToUserIdEntity userNameToUserId = await UserEntityUtils.GetUserNameToUserIdEntity(userName);

            UserEntity user = null;
            DeviceEntity device = null;

            if (userNameToUserId != null)
            {
                // We found this user in our mapping, so they are a Huddle user.  

                // Retrieve the current Huddle user information.
                user = await UserEntityUtils.GetUserEntity(userNameToUserId.UserId);
                string deviceId = loginMethod.parameters.deviceId;
                if (string.IsNullOrEmpty(deviceId))
                {
                    device = await CreateDeviceEntity(user.UserId);
                }
                else
                {
                    device = await UserEntityUtils.GetDeviceEntity(user.UserId, deviceId);
                }
            }
            else
            {
                // We haven't seen this username yet, so go create them.
                user = await CreateUser(userName);
                device = await CreateDeviceEntity(user.UserId);
            }

            if (user == null)
            {
                // Something went pretty wrong here, because we didn't find them and didn't create them.
                throw new ArgumentException("User not found");
            }

            if (device == null)
            {
                // Something went pretty wrong here, because we didn't find it and didn't create it.
                throw new ArgumentException("Device not found");
            }

            // Update the session state so we know who we are talking to for this session.
            sessionState.UserId = user.UserId;
            sessionState.DeviceId = device.DeviceId;
            sessionState.UserName = user.UserName;

            RoomEntity room = await RoomEntityUtils.GetCreateRoomEntity(loginMethod.parameters.roomName, sessionState, user);

            sessionState.RoomName = room.RoomName;

            RoundResponse roundResponse = await RoundResponse.GetCurrentRoundResponse(room);

            return new LoginResponse() { deviceId = sessionState.DeviceId, user = user, round = roundResponse };
        }

        /// <summary>
        /// Creates the user and updates some mappings.
        /// </summary>
        /// <param name="userName">The user name.</param>
        /// <returns>The new user table entity.</returns>
        private static async Task<UserEntity> CreateUser(string userName)
        {
            UserEntity newUser = await UserEntityUtils.CreateUserEntity(userName);

            // Now that we've created the user, add them to the username -> User ID mapping.
            await UserEntityUtils.SetUserNameToUserIdEntity(userName, newUser.UserId);

            return newUser;
        }

        private static async Task<DeviceEntity> CreateDeviceEntity(
           string userId)
        {
            CloudTable deviceTable = await GetCreateTable.GetTable("devices");

            string deviceId = "PDEV_" + Guid.NewGuid().ToString();

            DeviceEntity tempDeviceEntity = new DeviceEntity(userId, deviceId);
            TableOperation insertOperation = TableOperation.Insert(tempDeviceEntity);

            await deviceTable.ExecuteAsync(insertOperation);

            return tempDeviceEntity;
        }
    }
}
