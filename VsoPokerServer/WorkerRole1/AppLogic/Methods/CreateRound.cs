﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WorkerRole1.TableUtils;
using WorkerRole1.StorageTypes;
using HuddleAzureService.TableUtils;
using Microsoft.WindowsAzure.Storage.Table;
using System.Threading;
using WorkerRole1.ErrorHandling;

namespace WorkerRole1.AppLogic
{
    public partial class Methods
    {
        private class CreateRoundMethodParser
        {
            public string jsonrpc;
            public string method;

            [JsonProperty("params")]
            public CreateRoundParametersParser parameters;
        }

        private class CreateRoundParametersParser
        {
            public CreateRoundVSOItem item;
        }

        private class CreateRoundVSOItem
        {
            public string text;
            public string type;
        }

        public class CreateRoundResponse : IRpcResponse
        {
            public string roundId;
            public List<UserEntity> users;
            //public RoundEntity round;
        }

        /// <summary>
        /// Logs the user in, and, if necessary, creates the user in the user table.
        /// </summary>
        /// <param name="methodJson">The JSON representing the input parameters to the login method.</param>
        /// <param name="sessionState">The user's session state.</param>
        /// <param name="writer">The StreamWriter into which the response should be written.</param>
        /// <returns>A JSON-RPC response indicating success or failure.</returns>
        public static async Task<IRpcResponse> CreateRound(string methodJson, SessionState sessionState)
        {
            CreateRoundMethodParser createRoundMethod = (CreateRoundMethodParser)JsonConvert.DeserializeObject(methodJson, typeof(CreateRoundMethodParser));

            RoomEntity room = await RoomEntityUtils.GetRoomEntity(sessionState.RoomName);

            if (room == null)
            {
                throw new Exception("Current room could not be found: " + sessionState.RoomName);
            }

            if (room.CurrentRoundId != null)
            {
                RoundEntity currentRound = await RoundEntityUtils.GetRoundEntity(room.RoomName, room.CurrentRoundId);

                if (currentRound.IsOpen)
                {
                    throw new Exception("current room already has an active round: " + room.CurrentRoundId);
                }
            }

            string text = null;
            string type = null;

            if (createRoundMethod.parameters.item != null)
            {
                text = createRoundMethod.parameters.item.text;
                type = createRoundMethod.parameters.item.type;
            }

            RoundEntity round = await RoundEntityUtils.CreateRoundEntity(room, sessionState, type, text);

            room.CurrentRoundId = round.RoundId;

            await RoomEntityUtils.SaveRoomEntity(room);

            List<UserEntity> users = await UserEntityUtils.GetUserObjectsFromIds(room.GetUsers());

            NewRoundNotification notification = new NewRoundNotification(round.RoundId, users, createRoundMethod.parameters.item);

            MethodUtils.DispatchNotificationToUsersAsync(room.GetUsers(), sessionState, JsonConvert.SerializeObject(notification));

            return new CreateRoundResponse() { users = users, roundId = round.RoundId };
        }

        private class NewRoundNotification
        {
            public string method = "new_round";

            [JsonProperty("params")]
            public NewRoundNotificationParams parameters;

            public NewRoundNotification(string roundId, List<UserEntity> users, CreateRoundVSOItem vsoItem)
            {
                this.parameters = new NewRoundNotificationParams(roundId, users, vsoItem);
            }

        }

        private class NewRoundNotificationParams
        {
            public NewRoundNotificationParams(string roundId, List<UserEntity> users, CreateRoundVSOItem vsoItem)
            {
                this.users = users;
                this.roundId = roundId;
                this.item = vsoItem;
            }

            public string roundId;
            public List<UserEntity> users;
            public CreateRoundVSOItem item;
        }

    }
}
