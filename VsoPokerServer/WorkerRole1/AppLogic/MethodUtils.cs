﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WorkerRole1.ErrorHandling;
using WorkerRole1.TableUtils;

namespace WorkerRole1.AppLogic
{
    public class MethodUtils
    {
        private class RpcRequestParser
        {
            public string jsonrpc;
            public string id;
            public string method;
        }

        private class RpcResponseObject
        {
            public string jsonrpc = "2.0";
            public string id;
            public IRpcResponse result;
        }

        public static async Task ParseAndExecuteMethod(string methodJson, StreamWriter writer, SessionState sessionState)
        {
            RpcRequestParser rpcMethod = (RpcRequestParser)JsonConvert.DeserializeObject(methodJson, typeof(RpcRequestParser));

            RpcResponseObject response = new RpcResponseObject();

            response.id = rpcMethod.id;
            sessionState.CurrentRequestId = response.id;

            switch (rpcMethod.method)
            {
                case "login":
                    // TODO (peturner/aduong): Simple Login is test code, it uses a login name instead of an MSA auth token for testing.`
                    response.result = await Methods.Login(methodJson, sessionState, writer);
                    // Do this asynchronously -- no need to await this.
                    // Send the response for login before sending any messages in user queue
                    QueueResponseUtils.QueueResponseUtils.PushQueueOntoReponseStream(sessionState, writer, JsonConvert.SerializeObject(response)).ContinueWith(task =>
                    {
                        // If PushQueueOntoReponseStream throws an error we need to close the current session and handle any errors.
                        ErrorHandlingUtils.HandleGenericErrorAndCloseSession(sessionState, task.Exception, writer);
                    });
                    return;
                case "createRound":
                    response.result = await Methods.CreateRound(methodJson, sessionState);
                    break;
                case "voteOnRound":
                    response.result = await Methods.VoteOnRound(methodJson, sessionState);
                    break;
                case "closeRound":
                    response.result = await Methods.CloseRound(methodJson, sessionState);
                    break;
                default:
                    throw new ArgumentException("unrecognized JSON method: " + rpcMethod.method + " request made: " + methodJson);
            }

            await QueueResponseUtils.QueueResponseUtils.PushMessageOntoUserQueue(JsonConvert.SerializeObject(response), sessionState);
        }

        /// <summary>
        /// Dispatches a message notification to all the devices of all users (except the current device)
        /// </summary>
        /// <param name="huddleUsers">Users to send notifications to</param>
        /// <param name="sessionState">The current user/device session state</param>
        /// <param name="message">Message to be dispatched</param>
        public static async Task DispatchNotificationToUsersAsync(List<string> huddleUsers, SessionState sessionState, string message, bool pushToCurrentDevice = false)
        {
            IEnumerable<Task<List<string>>> getDeviceIdsTasks = from userId in huddleUsers
                                                                select UserEntityUtils.GetDevicesIdsForUser(userId);

            // Fetch all device Id's in parallel
            await Task.WhenAll(getDeviceIdsTasks.ToArray());

            List<string> allDeviceIdsForAllUsers = new List<string>();
            foreach (Task<List<String>> getDevicesForUserTask in getDeviceIdsTasks)
            {
                allDeviceIdsForAllUsers.AddRange(getDevicesForUserTask.Result);
            }

            if (!pushToCurrentDevice)
            {
                // Don't send a push notification to the current device
                allDeviceIdsForAllUsers.Remove(sessionState.DeviceId);
            }

            DispatchNotificationToDevicesAsync(allDeviceIdsForAllUsers, message);
        }

        /// <summary>
        /// Dispatches a message notification to all the devices of the users
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static async Task DispatchNotificationToUserAsync(String userId, string message)
        {
            List<string> allDeviceIdsForUser = await UserEntityUtils.GetDevicesIdsForUser(userId);

            DispatchNotificationToDevicesAsync(allDeviceIdsForUser, message);
        }

        /// <summary>
        /// Dispatches a message notification to all the devices in the list
        /// </summary>
        /// <param name="deviceIds">Devices to send notifications to</param>
        /// <param name="message">Message to be dispatched</param>
        public static void DispatchNotificationToDevicesAsync(List<string> deviceIds, string message)
        {
            foreach (String deviceId in deviceIds)
            {
                // Do this asyncronously for performance -- no need for await
                Task delayTask = QueueResponseUtils.QueueResponseUtils.PushMessageOntoUserQueue(message, deviceId);
            }
        }
    }
}
