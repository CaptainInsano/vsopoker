﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WorkerRole1.ErrorHandling;

namespace WorkerRole1
{
    /// <summary>
    /// The user's current session state.
    /// Everything in this class must be thread safe
    /// </summary>
    public class SessionState
    {
        public SessionState() { }

        public string UserId { get; set; }

        public StreamWriter StreamWriter { get; set; }

        public string DeviceId { get; set; }

        public string RoomName { get; set; }

        public string UserName { get; set; }

        public string CurrentRequestId { get; set; }

        public bool IsDisposed { get; set; }

        private object _clientLock = new object();

        private TcpClient _tcpClient;
        public TcpClient TcpClient { set { _tcpClient = value; } }

        // Use this to be able to cancel async queuing tasks if session is disposed 
        private CancellationTokenSource _cancellationTokenSource;

        public CancellationTokenSource CancellationTokenSource
        {
            set
            {
                _cancellationTokenSource = value;
            }
        }

        public CancellationToken CancellationToken
        {
            get
            {
                // http://msdn.microsoft.com/en-us/library/system.threading.cancellationtoken(v=vs.110).aspx
                // All public and protected members of CancellationToken are thread-safe and may be used concurrently from multiple threads
                return _cancellationTokenSource.Token;
            }
        }

        public void EndSession()
        {
            // http://msdn.microsoft.com/en-us/library/system.threading.cancellationtoken(v=vs.110).aspx
            // All public and protected members of CancellationToken are thread-safe and may be used concurrently from multiple threads.
            if (_cancellationTokenSource != null)
            {
                _cancellationTokenSource.Cancel();
            }

            IsDisposed = true;

            lock (_clientLock)
            {
                if (_tcpClient != null)
                {
                    try
                    {
                        _tcpClient.Close();
                    }
                    catch { }
                }
            }

            UserDisconnectUtils.HandleUserDisconnect(this).Wait();
        }
    }
}
