using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using HuddleAzureService.TableUtils;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Newtonsoft.Json.Linq;
using WorkerRole1.AppLogic;
using WorkerRole1.ErrorHandling;
using WorkerRole1.NetworkUtils;
using WorkerRole1;
using System.Runtime.InteropServices;

namespace HuddleAzureService
{
    public class WorkerRole : RoleEntryPoint
    {
        private static volatile Boolean _onStopCalled = false;
        private static volatile Boolean _returnedFromRunMethod = false;

        private static async Task ProcessRequestAsync(TcpClient client)
        {
            NetworkStream networkStream = client.GetStream();

            // http://blogs.msdn.com/b/cie/archive/2014/02/14/connection-timeout-for-windows-azure-cloud-service-roles-web-worker.aspx
            // http://social.msdn.microsoft.com/Forums/windowsazure/en-US/b3befafc-e8bd-427e-8da1-15cc26f6f225/limitations-of-opened-tcp-sockets-in-a-worker-role
            // Azure load balancers silently drop TCP connections after 1 minute if there's no network traffic (packets don't get delivered).  Send keepalives every 40sec.
            client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
            SetKeepAlive(client.Client, true, 40000, 40000);

            StreamReader reader = new StreamReader(networkStream);
            StreamWriter writer = new StreamWriter(networkStream);

            SessionState sessionState = new SessionState();
            sessionState.TcpClient = client;

            sessionState.CancellationTokenSource = new CancellationTokenSource();

            while (!_onStopCalled)
            {
                try
                {
                    await MethodUtils.ParseAndExecuteMethod(await NetworkParsingUtils.GetJsonObjectFromStream(reader), writer, sessionState);
                }
                catch (Exception e)
                {
                    try
                    {
                        // If HandleGenericError throws then there's something wrong with the network, end the session
                        ErrorHandlingUtils.HandleGenericError(sessionState, e, writer);
                    }
                    catch
                    {
                        sessionState.EndSession();
                        return;
                    }
                }
            }

            sessionState.EndSession();
        }

        // http://stackoverflow.com/questions/2127943/tcpip-networking-with-c-sharp
        private static void SetKeepAlive(Socket socket, bool on, uint time, uint interval)
        {
            /* the native structure
            struct tcp_keepalive {
            ULONG onoff;
            ULONG keepalivetime;
            ULONG keepaliveinterval;
            };
            */

            // marshal the equivalent of the native structure into a byte array
            uint dummy = 0;
            var inOptionValues = new byte[Marshal.SizeOf(dummy) * 3];
            BitConverter.GetBytes((uint)(on ? 1 : 0)).CopyTo(inOptionValues, 0);
            BitConverter.GetBytes((uint)time).CopyTo(inOptionValues, Marshal.SizeOf(dummy));
            BitConverter.GetBytes((uint)interval).CopyTo(inOptionValues, Marshal.SizeOf(dummy) * 2);
            // of course there are other ways to marshal up this byte array, this is just one way

            // call WSAIoctl via IOControl
            socket.IOControl(IOControlCode.KeepAliveValues, inOptionValues, null);

        }

        static async Task ProcessRequestsAsync(TcpListener listener)
        {
            while (!_onStopCalled)
            {
                TcpClient tcpClient = await listener.AcceptTcpClientAsync();

                // Execute asyncronously
                ProcessRequestAsync(tcpClient);
            }

            // TODO (peturner): Allow all pending tasks to complete

            _returnedFromRunMethod = true;
        }

        /// <summary>
        /// Main execution method, do not return from this method or the service will stop
        /// </summary>
        public override void Run()
        {
            TcpListener listener = new TcpListener(RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["PokerServiceEndpoint"].IPEndpoint);

            listener.Start();

            ProcessRequestsAsync(listener).Wait();
        }

        /// <summary>
        /// Called when the service is started, initialization tasks should go here.
        /// </summary>
        /// <returns></returns>
        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections 
            ServicePointManager.DefaultConnectionLimit = 1000;

            IEnumerable<Task> tableCreationTasks = from tableName in _tablesToCreate
                                                   select GetCreateTable.CreateIfNotExist(tableName);

            Task.WaitAll(tableCreationTasks.ToArray());

            return base.OnStart();
        }

        /// <summary>
        /// Called when the service is shut down, the code here gives us some time to clean up gracefully.
        /// </summary>
        public override void OnStop()
        {
            _onStopCalled = true;
            while (_returnedFromRunMethod == false)
            {
                System.Threading.Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// Storage tables to try and create when the service starts
        /// </summary>
        private static List<string> _tablesToCreate = new List<string>()
        {
            "userNameToPusr", // mapping from username to Poker User ID (for "simple login")
            "users",
            "devices",
            "rounds",
            "rooms",
            "userToRoundsIndex",
        };
    }
}
