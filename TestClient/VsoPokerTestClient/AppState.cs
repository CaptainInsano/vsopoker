﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace VsoPokerTestClient
{
    public static class AppState
    {
        public static volatile TcpClient Client;

        private static volatile StreamReader _reader;
        private static volatile StreamWriter _writer;

        public static void Clear()
        {
            if (Client != null)
            {
                try
                {
                    Client.Close();
                }
                catch { }

                Client = null;
            }
            _reader = null;
            _writer = null;
        }

        public static StreamReader Reader
        {
            get
            {
                if (_reader != null)
                {
                    return _reader;
                }

                _reader = new StreamReader(Client.GetStream());

                return _reader;
            }
        }

        public static StreamWriter Writer
        {
            get
            {
                if (_writer != null)
                {
                    return _writer;
                }

                _writer = new StreamWriter(Client.GetStream());

                return _writer;
            }
        }
    }
}
