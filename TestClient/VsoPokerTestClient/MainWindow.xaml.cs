﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace VsoPokerTestClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        /// <summary>
        /// Open Connecion Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs eargs)
        {
            int port = int.Parse(serverPort.Text);
            string server = serverUrl.Text;

            AppState.Clear();

            try
            {
                AppState.Client = new System.Net.Sockets.TcpClient();
                AppState.Client.Connect(server, port);
                responseText.Text += "connected to " + server + ":" + port + "\n";
            }
            catch (Exception e)
            {
                responseText.Text += "exception: " + e.Message;
                return;
            }

            ReadFromInputStream(AppState.Reader, responseText);
        }

        private static async Task ReadFromInputStream(StreamReader reader, TextBox textBlock)
        {
            try
            {
                while (true)
                {
                    string jsonResponse = await NetworkParsingUtils.GetJsonObjectFromStream(reader);

                    textBlock.Text += JsonConvert.SerializeObject(JsonConvert.DeserializeObject(jsonResponse), Formatting.Indented) + "\n";
                }
            }
            catch (Exception e)
            {
                textBlock.Text += "\nException Occured:" + e.Message + "\n";
            }
            finally
            {
                AppState.Clear();
                textBlock.Text += "\ndisconnected!\n";
            }
        }
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // ... Get the ComboBox.
            var comboBox = sender as ComboBox;
            ComboBoxItem typeItem = (ComboBoxItem)comboBox.SelectedItem;
            string value = typeItem.Content.ToString();
            switch (value)
            {
                case "Login":
                    Login();
                    break;
                case "CreateRound":
                    CreateRound();
                    break;
                case "VoteOnRound":
                    VoteOnRound();
                    break;
                case "CloseRound":
                    CloseRound();
                    break;
            }
        }

        private void Login()
        {
            string loginJson =
    @"{ 
    jsonrpc: ""2.0"",
	id : 2,
	method: ""login"",
	params: 
	{
		userName:""" + userId.Text + @""",
        roomName:""huddletest""
	}
}";

            setRequestPreviewText(loginJson);
        }


        private void CreateRound()
        {
            string json =
    @"{ 
    jsonrpc: ""2.0"",
	id : 2,
	method: ""createRound"",
	params: 
	{
		item: 
        {
            text: ""face"",
            type: ""VsoFaceItem""
        }
	}
}";

            setRequestPreviewText(json);
        }

        private void VoteOnRound()
        {
            string json =
    @"{ 
    jsonrpc: ""2.0"",
	id : 2,
	method: ""voteOnRound"",
	params: 
	{
		roundId: ""someRoundId"",
        voteVal: 8
	}
}";

            setRequestPreviewText(json);
        }

        private void CloseRound()
        {
            string json =
    @"{ 
    jsonrpc: ""2.0"",
	id : 2,
	method: ""closeRound"",
	params: 
	{
		roundId: ""someRoundId""
	}
}";

            setRequestPreviewText(json);
        }


        private void requestText_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void serverPort_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void responseText_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void requestPreview_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void setRequestPreviewText(string requestText)
        {
            requestPreview.Text = JsonConvert.SerializeObject(JsonConvert.DeserializeObject(requestText), Formatting.Indented);
        }

        private void executeRequest_Click(object sender, RoutedEventArgs e)
        {
            string requestJson = requestPreview.Text;

            requestText.Text += requestJson + "\n";

            AppState.Writer.Write(requestJson);
            AppState.Writer.Flush();
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            AppState.Clear();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            requestText.Text = "";
            responseText.Text = "";
        }
    }
}
